const UserHeaderTable = [
  'Nombre',
  'Cargos',
  'Sucursales',
  'Horas/semana',
  'Editar',
];
const userModel = {
  id: '',
  email: '',
  firstName: '',
  lastName: '',
  dateOfBirth: '',
  employeeId: '',
  contract: '',
};
export default { UserHeaderTable, userModel };
