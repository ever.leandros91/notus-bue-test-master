import UserHeaderTable from './user';
import LocationHeaderTable from './location';
import turnTemplateHeaderTable from './turn-templates';

export default {
  ...UserHeaderTable,
  ...LocationHeaderTable,
  ...turnTemplateHeaderTable,
  apiUrl: 'http://localhost:3004/',
};
