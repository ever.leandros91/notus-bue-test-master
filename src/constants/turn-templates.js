const turnTemplateHeaderTable = [
  'Nombre',
  'Hora de entrada',
  'Hora de salida',
  'Descanso',
  'Horas Trabajadas',
  'Cargos',
  'Sucursales',
];

export default { turnTemplateHeaderTable };
