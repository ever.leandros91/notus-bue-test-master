import utils from './utils';
const state = {
  list: [],
};
const mutations = {
  setContract(state, contractList) {
    state.list = contractList;
  },
};
const getters = {
  list(state) {
    return state.list;
  },
};
const actions = {
  FETCH_CONTRACT({ commit }) {
    return new Promise((resolve, reject) => {
      fetch(`${utils.apiUrl}contracts`)
        .then((response) => response.json())
        .then((data) => {
          commit('setContract', data);
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
