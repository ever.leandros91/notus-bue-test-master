import utils from './utils';
const state = {
  list: [],
};
const mutations = {
  setPosition(state, positionList) {
    state.list = positionList;
  },
};
const getters = {
  list(state) {
    return state.list;
  },
};
const actions = {
  FETCH_POSITION({ commit }) {
    return new Promise((resolve, reject) => {
      fetch(`${utils.apiUrl}positions`)
        .then((response) => response.json())
        .then((data) => {
          commit('setPosition', data);
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
