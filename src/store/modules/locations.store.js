import utils from './utils';
const state = {
  list: [],
};
const mutations = {
  setLocation(state, locationList) {
    state.list = locationList;
  },
};
const getters = {
  list(state) {
    return utils.orderByAsc([...state.list], 'name') || [];
  },
};
const actions = {
  FETCH_LOCATION({ commit }) {
    return new Promise((resolve, reject) => {
      fetch(`${utils.apiUrl}locations`)
        .then((response) => response.json())
        .then((data) => {
          commit('setLocation', data);
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
