import utils from './utils';

const state = {
  list: [],
};
const mutations = {
  setTurns(state, turnsList) {
    state.list = turnsList;
  },
};
const getters = {
  list(state) {
    return state.list;
  },
  listUnassigned(state) {
    const list = state.list.filter(({ date, userId, maxSlots }) => {
      const acceptedDate = utils.dateCheck('2021-04-19', '2021-04-30', date);

      const unassigned = (userId || []).length < maxSlots;
      return unassigned && acceptedDate;
    });
    return utils.groupBy(list, 'date');
  },
  listAssigned(state) {
    state.list.filter(({ date }) => {
      return utils.dateCheck('19-04-2021', '30-04-2021', date);
    });
    return state.list;
  },
};
const actions = {
  FETCH_TURNS({ commit }) {
    return new Promise((resolve, reject) => {
      fetch(`${utils.apiUrl}turns`)
        .then((response) => response.json())
        .then((data) => {
          commit('setTurns', data);
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
