import utils from './utils';
const state = {
  list: [],
};
const mutations = {
  setTurnsTemplate(state, turnsList) {
    state.list = turnsList;
  },
};
const getters = {
  list(state) {
    return state.list;
  },
};
const actions = {
  FETCH_TURNS_TEMPLATE({ commit }) {
    return new Promise((resolve, reject) => {
      fetch(`${utils.apiUrl}turnTemplates`)
        .then((response) => response.json())
        .then((data) => {
          commit('setTurnsTemplate', data);
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
