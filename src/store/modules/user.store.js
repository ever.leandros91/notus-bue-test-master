import utils from './utils';
const state = {
  list: [],
};
const mutations = {
  setUsers(state, newUserList) {
    state.list = newUserList;
  },
  addUser(state, newUser) {
    state.list.splice(0, 0, newUser);
  },
  updateUser(state, user) {
    const index = state.list.findIndex(({ id }) => id === user.id);
    state.list.splice(index, 1, user);
  },
};
const getters = {
  list(state) {
    return [...state.list].sort((a, b) =>
      a.employeeId > b.employeeId ? 1 : b.employeeId > a.employeeId ? -1 : 0
    );
  },
};
const actions = {
  FETCH_USERS({ commit }) {
    return new Promise((resolve, reject) => {
      fetch(`${utils.apiUrl}users`)
        .then((response) => response.json())
        .then((data) => {
          commit('setUsers', data);
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  CREATE_USER({ commit, state }, user) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const existResponse = utils.verifyUserExist(state.list, user);
        if (existResponse) {
          reject(existResponse);
        } else {
          commit('addUser', user);
          resolve({ success: true });
        }
      }, 1000);
    });
  },
  UPDATE_USER({ commit }, user) {
    return new Promise((resolve) => {
      setTimeout(() => {
        commit('updateUser', user);
        resolve({ success: true });
      }, 1000);
    });
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
