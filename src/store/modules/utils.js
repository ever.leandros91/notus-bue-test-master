const dateCheck = (dateFrom, dateTo, dateCheck) => {
  try {
    const d1 = dateFrom.split('-');
    const d2 = dateTo.split('-');
    const c = dateCheck.split('-');

    const from = new Date(d1[0], parseInt(d1[1]) - 1, d1[2]); // -1 because months are from 0 to 11
    const to = new Date(d2[0], parseInt(d2[1]) - 1, d2[2]);
    const check = new Date(c[0], parseInt(c[1]) - 1, c[2]);
    return check > from && check < to;
  } catch (e) {
    return false;
  }
};
const orderByAsc = (list, key) => {
  return list.sort((a, b) => (a[key] > b[key] ? 1 : b[key] > a[key] ? -1 : 0));
};
const verifyUserExist = (list, user) => {
  const existMessageError = 'Este campo debe ser único';
  let existEmployeeId = false;
  let existId = false;

  const exist = list.find(({ employeeId, id }) => {
    existEmployeeId = user.employeeId === employeeId;
    existId = user.id === id;
    return existId || existEmployeeId;
  });
  if (exist) {
    const employeeId = existEmployeeId ? { employeeId: existMessageError } : {};
    const id = existId ? { id: existMessageError } : {};
    return { ...id, ...employeeId };
  }
  return false;
};
const groupBy = (data, group) => {
  return (data || []).reduce(function (r, a) {
    r[a[group]] = r[a[group]] || [];
    r[a[group]].push(a);
    return r;
  }, Object.create({}));
};

export default {
  apiUrl: 'http://localhost:3004/',
  dateCheck,
  verifyUserExist,
  groupBy,
  orderByAsc,
};
