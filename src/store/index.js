import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import user from './modules/user.store';
import contract from './modules/contract.store';
import position from './modules/positions.store';
import location from './modules/locations.store';
import turnsTemplate from './modules/turns-template.store';
import turns from './modules/turns.store';

Vue.use(Vuex);
const debug = process.env.NODE_ENV !== 'production';
export default new Vuex.Store({
  modules: {
    user,
    contract,
    position,
    location,
    turnsTemplate,
    turns,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});
