import Vue from 'vue';
import appBtn from '../components/app/btn';
import appLogo from '../components/app/logo';
import appTable from '../components/app/table/index';
import appTableTd from '../components/app/table/td';
import appTableTbody from '../components/app/table/tbody';
import appDialog from '../components/app/dialog';
import appTextField from '../components/app/text-field';
import appRow from '../components/app/row';
import appCol from '../components/app/col';
import appSelect from '../components/app/select/inedx';
import appSelectItem from '../components/app/select/item';
import appSelectSelected from '../components/app/select/selected';
Vue.component('appBtn', appBtn);
Vue.component('appLogo', appLogo);
Vue.component('appTable', appTable);
Vue.component('appTableTd', appTableTd);
Vue.component('appTableTbody', appTableTbody);
Vue.component('appDialog', appDialog);
Vue.component('appTextField', appTextField);
Vue.component('appRow', appRow);
Vue.component('appCol', appCol);
Vue.component('appSelect', appSelect);
Vue.component('appSelectItem', appSelectItem);
Vue.component('appSelectSelected', appSelectSelected);
