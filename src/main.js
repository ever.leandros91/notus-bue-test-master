import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './assets/tailwind.css';
// transitions
import './assets/transitions/fade.css';
import './assets/transitions/fade-absolute.css';
import './plugins';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
