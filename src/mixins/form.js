export default {
  data: function () {
    return {
      rules: {
        required: (value) => !!value || 'Este campo es requerido',
        email: (value) => {
          const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return !value || pattern.test(value) || 'email no válido';
        },

        date: (value) => {
          const regEx = /^\d{4}-\d{2}-\d{2}$/;
          const copy = value + ''.trim();
          if (!copy.match(regEx)) return 'fecha no válida'; // Invalid format
          const d = new Date(copy);
          const dNum = d.getTime();
          if (!dNum && dNum !== 0) return 'fecha no válida'; // NaN value, Invalid date
          return d.toISOString().slice(0, 10) === copy || 'fecha no válida';
        },
      },
    };
  },
};
