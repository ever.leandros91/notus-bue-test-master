import { mapActions, mapGetters } from 'vuex';
export default {
  computed: {
    ...mapGetters({
      userList: 'user/list',
      turnsTemplateList: 'turnsTemplate/list',
      contractList: 'contract/list',
      positionList: 'position/list',
      turnsListAssigned: 'turns/listAssigned',
      turnsLisUnassigned: 'turns/listUnassigned',
      turnsList: 'turns/list',
      locationList: 'location/list',
    }),
  },
  methods: {
    ...mapActions({
      fetchUserListAction: 'user/FETCH_USERS',
      fetchContractListAction: 'contract/FETCH_CONTRACT',
      fetchPositionListAction: 'position/FETCH_POSITION',
      fetchTurnsTemplateListAction: 'turnsTemplate/FETCH_TURNS_TEMPLATE',
      fetchTurnsListAction: 'turns/FETCH_TURNS',
      fetchLocationListAction: 'location/FETCH_LOCATION',
    }),
  },
};
